<?php
/**
 * @author: Fabian Rolof <fabian@rolof.pl>
 */
namespace sc0rp\tools;

use \Yii;

class Model extends \yii\db\ActiveRecord
{

  public function beforeValidate() {

    if ($this->isNewRecord && array_key_exists('created_by', $this->getAttributes()) && ($this->created_by === null)) {
      if (isset(Yii::$app->user) && Yii::$app->user->getIdentity()) {
        $this->created_by = Yii::$app->user->getIdentity()->username;
      } else {
        $this->created_by = 'SYSTEM';
      }
    } elseif (!$this->isNewRecord && array_key_exists('updated_by', $this->getAttributes())) {
      if (isset(Yii::$app->user) && Yii::$app->user->getIdentity()) {
        $this->updated_by = Yii::$app->user->getIdentity()->username;
      } else {
        $this->updated_by = 'SYSTEM';
      }
    }

    if ($this->isNewRecord && array_key_exists('created_ts', $this->getAttributes()) && ($this->created_ts === null)) {
      $this->created_ts = date('Y-m-d H:i:s');
    } elseif (!$this->isNewRecord && array_key_exists('updated_ts', $this->getAttributes())) {
      $this->updated_ts = date('Y-m-d H:i:s');
    }

    return parent::beforeValidate();
  }

  public function link($name, $model, $extraColumns = [])
  {
    if (!in_array('created_by', $extraColumns) || in_array('created_ts', $extraColumns)) {
      $relation = $this->getRelation($name);
      if (is_array($relation->via)) {
        $modelClass = $relation->via[1]->modelClass;
        if (class_exists($modelClass) && (in_array('getTableSchema', get_class_methods($modelClass)))) {
          if ($modelClass::getTableSchema()->getColumn('created_by')) {
            if (isset(Yii::$app->user)) {
              $extraColumns['created_by'] = Yii::$app->user->getIdentity()->username;
            } else {
              $extraColumns['created_by'] = 'SYSTEM';
            }
          }

          if ($modelClass::getTableSchema()->getColumn('created_ts')) {
            $extraColumns['created_ts'] = date('Y-m-d H:i:s');
          }
        }
      }
    }
    return parent::link($name, $model, $extraColumns);
  }

  public function save($runValidation = true, $attributeNames = null) {
    return parent::save($runValidation, $attributeNames);
  }
}
