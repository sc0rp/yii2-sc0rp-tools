<?php
namespace sc0rp\tools;

class Controller extends \yii\web\Controller {
    public function init() {
        if (isset(\Yii::$app->params['appName'])) $this->view->title = \Yii::$app->params['appName'];
        return parent::init();
    }
}