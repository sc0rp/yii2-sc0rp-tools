<?php

namespace sc0rp\tools;

/**
 * This is just an example.
 */
class Migration extends \yii\db\Migration
{

    private $columnsInfoAboutChanger = [
        'created_by' => 'varchar(255) not null',
        'created_ts' => 'datetime not null',
        'updated_by' => 'varchar(255) null',
        'updated_ts' => 'datetime null'
    ];

    protected function msg($str,$end='') {
        if ($end=='') echo "[" . date('Y-m-d H:i:s') . "] ";
        echo $str;
        if ($end!='') echo $end;
    }

    protected function prompt($message,$default=null)
    {
        if($default!==null) $message.=" [$default] ";
        else $message.=' ';

        if(extension_loaded('readline')) {
            $input=readline($message);
            if($input!==false) readline_add_history($input);
        }
        else {
            echo $message;
            $input=fgets(STDIN);
        }

        if($input===false) return false;
        else {
            $input=trim($input);
            return ($input==='' && $default!==null) ? $default : $input;
        }
    }

    public function createTable($table, $columns, $options = null) {
        $flag = true;
        if (isset($options['addInfoAboutChanger'])) {
            if (!$options['addInfoAboutChanger']) {
                $flag = false;
            }
            unset($options['addInfoAboutChanger']);
        }

        if ($flag) {
            foreach ($this->columnsInfoAboutChanger as $newColumn => $definition) {
                if (!isset($columns[$newColumn])) {
                    $columns[$newColumn] = $definition;
                }
            }
        }

        return parent::createTable($table, $columns, $options);
    }

}
