<?php
namespace sc0rp\tools;

trait Console {
    protected function msgInLine($msg) {
        $fill = strlen($msg) % 40;
        $msg .= str_repeat(" ", $fill);
        echo "\r" . $msg;

    }

    protected function msg($msg) {
        echo $msg;
    }
}