Yii2 Sc0rp tools
================
useful tools for Yii2

Installation
------------
tets
The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist sc0rp/yii2-sc0rp-tools "*"
```

or add

```
"sc0rp/yii2-sc0rp-tools": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \sc0rp\tools\AutoloadExample::widget(); ?>```