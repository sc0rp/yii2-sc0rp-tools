<?php
namespace sc0rp\tools;

use Faker\Provider\File;
use yii\base\Component;
use Yii;
use yii\base\Exception;
use yii\helpers\FileHelper;
use yii\web\HttpException;


class Tools {
    public static function getRoleUsers($role_name)
    {
        $connection = \Yii::$app->db;
        $connection->open();

        $command = $connection->createCommand(
            "SELECT * FROM auth_assignment INNER JOIN user ON auth_assignment.user_id = user.id " .
            "WHERE auth_assignment.item_name = '" . $role_name . "';");

        $users = $command->queryAll();
        $connection->close();

        return $users;
    }

    public static function isMenuActive($pattern) {
        $action = Yii::$app->controller->action->getUniqueId();
        if (strpos($action, $pattern) !== false) return true;
        return false;
    }

    public static function getRolesList($userId = null) {
        if ($userId !== null) $roles = \Yii::$app->authManager->getRolesByUser($userId);
        else $roles = \Yii::$app->authManager->getRoles();
        $return =  array();
        foreach ($roles as $roleName => $role) $return[$roleName] = $role->name;
        return $return;
    }

    public static function assignRoleToUser($userId, $roles) {
        if (is_array($roles)) foreach ($roles as $role) \Yii::$app->authManager->assign(
            \Yii::$app->authManager->getRole($role),
            $userId
        );
    }

    public static function clearRolesFromUser($userId) {
        \Yii::$app->authManager->revokeAll($userId);
    }

    public static function removeDiacriticsChars($string) {
        $chars = array(
            'ę' => 'e', 'Ę' => 'E', 'ó' => 'o', 'Ó' => 'O', 'ą' => 'a', 'Ą' => 'A', 'ś' => 's', 'Ś' => 'S',
            'ł' => 'l', 'Ł' => 'L', 'ż' => 'z', 'Ż' => 'Z', 'ź' => 'z', 'Ź' => 'z', 'ć' => 'c', 'Ć' => 'C',
            'ń' => 'n', 'Ń' => 'N'
        );
        return strtr($string, $chars);
    }

    public static function createTmpDir($subDir = null) {
        if ($subDir != null) {
            $subDir .= DIRECTORY_SEPARATOR;
        }
        $dir = Yii::getAlias('@runtime') . DIRECTORY_SEPARATOR . $subDir . Yii::$app->security->generateRandomString();
        if (is_dir($dir)) return self::createTmpDir();
        mkdir($dir, 0777, true);
        return $dir;
    }

    public static function removeUtf8Bom($text) {
        $bom = pack('H*','EFBBBF');
        $text = preg_replace("/^$bom/", '', $text);
        return $text;
    }

    public static function getRootPath() {
        return Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR;
    }

    public static function runCommand($command) {
        if (substr(php_uname(),0,7)=='Windows') {
            $WshShell = new \COM("WScript.Shell");
            $oExec = $WshShell->Run("cmd /c " . $command, 0, false);
        } else {
            $outputfile = '/dev/null';
            $pidfile = '/dev/null';
            exec(sprintf("%s > %s 2>&1 & echo $! >> %s", $command, $outputfile, $pidfile));
        }
    }

    public static function getUniqueFileName($directory, $extension) {
        $randomName = Yii::$app->security->generateRandomString() . "." . $extension;
        if (!is_dir($directory)) {
            throw new HttpException(500, 'Directory:' . $directory . ' not found.');
        }

        if (is_file($directory . DIRECTORY_SEPARATOR .$randomName)) {
            return self::getUniqueFileName($directory, $extension);
        }
        return $randomName;
    }
}